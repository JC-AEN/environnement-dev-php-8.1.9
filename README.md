# Environnement de Développement PHP 8.1.9

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement en PHP 8.1.9. Une fois démarré, vous pourrez créer un projet Symfony 6

### Contenu
Dans ce container, vous trouverez des images de :
- [PHP 8.1.9](https://hub.docker.com/_/php).
- [MySql](https://hub.docker.com/_/mysql).
- [PhpMyAdmin](https://hub.docker.com/_/phpmyadmin).
- [MailDev](https://hub.docker.com/r/maildev/maildev).

## Prérequis
Pour utiliser ce container de manière simple, télécharger et lancer le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/environnement-dev-php-8.1.9.git
```

Puis positionnez-vous dans ce dépot et créez l'image docker.
```
cd environnement-dev-php-8.1.9
docker-compose up -d
```

Vous pouvez vérifier si tout est bien en tapant
```
docker ps
```

ou directement sur `Docker Desktop`, dans l'onglet `Containers`.

### Vérification des services lancés
Pour vérifier que tous les services soient bien lancés, nous allons tester quelques commandes à l'intérieur du container. Pour l'instant, votre invite de commande est situé dans le dossier et pas dans le container. Pour se positionner au bon endroit, voici la commande
```
docker exec -it www_docker_symfony bash
```
Nous sommes maintenant positionné dans le dossier `/var/www` du container. 

Pour vérifier la version de `PHP`, et par la même occasion, voir si le serveur `Apache` est bien lancé, tapez
```
php -v
```
Vous devriez avoir la version 8.1.9 de `PHP`

Vous allez ensuite tester la version de `composer`
```
composer -V
```
Vous devriez avoir la version 2.4.0 de `composer`

Il ne reste plus qu'a tester le serveur `MySQL` et `PhpMyAdmin`. Pour cela, dans votre navigateur, rendez vous à l'adresse [http://localhost:8080/](http://localhost:8080/) et saisissez `root` pour l'utilisateur sans mot de passe. Si vous arrivez à vous connecter à `PhpMyAdmin`, c'est que le serveur `MySQL` est bien lancé.
Une autre solution consiste à lancer cette page directement depuis `Docker Desktop`. Dans le container `phpmyadmin_docker_symfony`, cliquez sur `OPEN WITH BROWSER`.

### Création d'un projet Symfony 6
Pour créer un projet `Symfony 6` dans cette environnement, lancer une commande `composer` depuis le container
```
composer create-project symfony/website-skeleton project
```
> :warning: **Si vous voulez changer le nom du projet ou installer plusieurs projets**: Il vous faudra modifier le fichier [vhosts.conf](/php/vhosts/vhosts.conf).

Tapez
```
cd project
```
Vous voilà maintenant dans votre projet `Symfony`, le serveur étant déjà lancé, il ne vous reste plus qu'a aller à l'adresse [http://localhost:8741/](http://localhost:8741/) pour arriver sur la page d'accueil de votre projet.

> :warning: **Symfony CLI ne fonctionne pas**: Pour lancer des commandes `Symfony` sans ce service, utilisez la commande 
```
php bin/console ...
```

Bon développement :sunglasses:

# Liste des fonctionnalités à ajouter

### Todo

- [ ] Ajouter l'installation de `Symfony CLI` au fichier [Dockerfile](/php/Dockerfile).  
  - [ ] Mettre à jour le README.md  
- [ ] Ajouter l'installation de `NodeJs` au fichier [Dockerfile](/php/Dockerfile).  
  - [ ] Mettre à jour le README.md  

### In Progress


### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  